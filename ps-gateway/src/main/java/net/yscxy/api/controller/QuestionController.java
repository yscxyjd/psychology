package net.yscxy.api.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import lombok.extern.log4j.Log4j;
import net.yscxy.service.config.ServerResponse;
import net.yscxy.service.entity.AnswerResult;
import net.yscxy.service.service.IAnswerResultService;
import net.yscxy.service.service.IQuestionGroupService;
import net.yscxy.service.service.IQuestionService;
import net.yscxy.service.service.ITypeBarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangfukun
 */
@RestController
@RequestMapping("/question")
@Log4j
public class QuestionController {
    @Reference(version = "1.0.0")

    IQuestionService questionService;

    @Reference(version = "1.0.0")
    IQuestionGroupService questionGroupService;

    @Reference(version = "1.0.0")
    ITypeBarService typeBarService;

    @Reference(version = "1.0.0")
    IAnswerResultService answerResultService;

    /**
     * 主页获取所有的分类列表
     *
     * @return
     */
    @RequestMapping("/listTab")
    public ServerResponse<?> getTab() {
        System.out.println("进来了！");
        return typeBarService.getAllTypeBar();
    }

    /**
     * 主页获取所有的问题列表
     *
     * @return
     */
    @RequestMapping("/listGroup")
    public ServerResponse<?> getQuestionGroups() {
        return questionGroupService.getQuestionGroupPage();
    }

    /**
     * 获取问题列表下的所有问题的信息
     *
     * @return
     */
    @RequestMapping("/listQuestions")
    public ServerResponse<?> getGroupQuestions(Integer groupId) {
        return questionService.getAllQuestions(groupId);
    }

    /**
     * 根据分类获取分类下的列表
     *
     * @return
     */
    @RequestMapping("/getGroupQuestionsByGroupId")
    public ServerResponse<?> getGroupQuestionsByGroupId(Integer groupId) {
        System.out.println("groupId:" + groupId);
        return questionGroupService.getQuestionGroupById(groupId);
    }

    /**
     * 根据分类获取分类下的列表
     *
     * @return
     */
    @RequestMapping("/getQuestionByName")
    public ServerResponse<?> getQuestionByName(String name) {
        return questionGroupService.getByName(name);
    }


    /**
     * 查询答题记录信息
     *
     * @return
     */
    @RequestMapping("/getAnswerResult")
    public ServerResponse<?> getAnswerResult() {
        if (!StpUtil.isLogin()) {
            return ServerResponse.createByErrorMesage("失败");
        }
        log.info("当前用户已登录");
        return answerResultService.getAnsterResult(StpUtil.getLoginId(3L));
    }


}
