package net.yscxy.api.controller;


import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import lombok.extern.log4j.Log4j;
import net.yscxy.service.config.ServerResponse;
import net.yscxy.service.entity.SysUser;
import net.yscxy.service.entity.User;
import net.yscxy.service.service.SysUserService;
import net.yscxy.service.serviceImpl.SysUserServiceImpl;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.*;

@RestController
@RequestMapping("/user")
@Log4j
public class LoginController {

    @Reference(version = "1.0.0")
    private SysUserService userService;

    @RequestMapping("/getUser")
    public ServerResponse getUser(Long userId) {
        return userService.getUserMsgByUserId(userId);
    }

    @PostMapping("/login")
    public ServerResponse<?> login(Long phone, String pwd) {
        System.out.println(phone);
        System.out.println(pwd);
        System.out.println("登录！");
        if (Objects.isNull(phone) || Objects.isNull(pwd)) {
            return ServerResponse.createByErrorMesage("账户或密码错误");
        }
        ServerResponse<SysUser> stringServerResponse = userService.userLogin(phone, pwd);
        SysUser user = stringServerResponse.getData();
        if (Objects.nonNull(user)) {
            StpUtil.login(user.getUserId());
        }
        Map<String, Object> map = new HashMap<>();
        SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
        map.put("user", user);
        map.put("tokenInfo", tokenInfo);
        return ServerResponse.createBySuccess(map);
    }

    @PostMapping("/register")
    public ServerResponse<?> register(Long phone, String pwd) {
        log.info("注册参数:" + phone);
        Map<String, Object> map = new HashMap<>();
        map.put("phonenumber", phone);
        ServerResponse<SysUser> userByPhone = userService.getUserByPhone(phone.toString());
        if (Objects.nonNull(userByPhone.getData())) {
            return ServerResponse.createByErrorMesage("该手机号码已经注册！");
        }

        SysUser sysUser = new SysUser();
        Date now = new Date();
        sysUser = new SysUser();
        sysUser.setSalt(UUID.randomUUID().toString());
        sysUser.setDeptId(1L);
        sysUser.setUserName("openArt123");
        sysUser.setUserType("00");
        sysUser.setPhonenumber(String.valueOf(phone));
        sysUser.setAvatar("http://img.yscxy.net/testFolder/first.png");
        sysUser.setPassword(pwd);
        sysUser.setStatus("0");
        sysUser.setDelFlag("0");
        sysUser.setLoginIp("127.0.0.1");
        sysUser.setLoginDate(now);
        sysUser.setCreateBy("System");
        sysUser.setCreateTime(now);
        sysUser.setRemark("用户");
        userService.insert(sysUser);
        return ServerResponse.createBySuccess("注册成功");
    }
}
