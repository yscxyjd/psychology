package net.yscxy.service.service;

import net.yscxy.service.config.ServerResponse;
import net.yscxy.service.entity.Question;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ShaoXuan
 * @since 2022-04-05
 */
import com.alibaba.dubbo.config.annotation.Service;
import net.yscxy.service.entity.Question;
import net.yscxy.service.mapper.QuestionMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public interface IQuestionService extends IService<Question> {

    /**
     * 获取一个问题组的所有问题集合
     *
     * @param groupId
     * @return
     */
     ServerResponse<List<Question>> getAllQuestions(Integer groupId);


}
