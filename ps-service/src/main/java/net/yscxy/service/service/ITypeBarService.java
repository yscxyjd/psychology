package net.yscxy.service.service;

import net.yscxy.service.config.ServerResponse;
import net.yscxy.service.entity.TypeBar;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ShaoXuan
 * @since 2022-04-05
 */
import com.alibaba.dubbo.config.annotation.Service;

import java.util.List;

@Service
public interface ITypeBarService extends IService<TypeBar> {


    /**
     * 获取全部的typeBar
     *
     * @return 全部的typeBar
     */
    ServerResponse<List<TypeBar>> getAllTypeBar();


}
