package net.yscxy.service.service;

import net.yscxy.service.config.ServerResponse;
import net.yscxy.service.entity.QuestionGroup;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ShaoXuan
 * @since 2022-04-05
 */
import com.alibaba.dubbo.config.annotation.Service;

import java.util.List;

@Service
public interface IQuestionGroupService extends IService<QuestionGroup> {

    /**
     * 查询问题列表
     *
     * @return 分页数据
     */
    ServerResponse<List<QuestionGroup>> getQuestionGroupPage();

    /**
     * 根据分类查询问题列表组
     *
     * @param groupId
     * @return 分页数据
     */
    ServerResponse<List<QuestionGroup>> getQuestionGroupById(Integer groupId);

    /**
     * @param name 根据名称进行模糊查询
     * @return
     */
    ServerResponse<List<QuestionGroup>> getByName(String name);

}
