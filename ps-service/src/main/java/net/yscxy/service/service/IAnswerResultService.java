package net.yscxy.service.service;

import com.baomidou.mybatisplus.plugins.Page;
import net.yscxy.service.config.ServerResponse;
import net.yscxy.service.entity.AnswerResult;
import com.baomidou.mybatisplus.service.IService;
import net.yscxy.service.entity.QuestionGroup;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author ShaoXuan
 * @since 2022-04-05
 */
public interface IAnswerResultService extends IService<AnswerResult> {
    /**
     * 用户提交结果
     *
     * @param answerResult 问题
     * @return 是否成功
     */

    ServerResponse<Boolean> addAnswerResult(AnswerResult answerResult);

    /**
     * 获取用户的答题记录
     *
     * @param userId 用户id
     * @return 查询结果
     */
    ServerResponse<AnswerResult> getAnsterResult(Long userId);

}
