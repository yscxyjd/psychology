package net.yscxy.service.config;


public enum ResponseCode {
    /**
     * 成功code
     */
    SUCCESS(0, "SUCCESS"),
    /**
     * 失败code
     */
    ERROR(1, "ERROR");


    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    private final int code;
    private final String desc;

    ResponseCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

}
