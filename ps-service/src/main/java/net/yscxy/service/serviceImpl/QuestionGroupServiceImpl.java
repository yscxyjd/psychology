package net.yscxy.service.serviceImpl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import net.yscxy.service.config.ServerResponse;
import net.yscxy.service.entity.QuestionGroup;
import net.yscxy.service.mapper.QuestionGroupMapper;
import net.yscxy.service.service.IQuestionGroupService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ShaoXuan
 * @since 2022-04-05
 */
@Service
@com.alibaba.dubbo.config.annotation.Service(version = "1.0.0", timeout = 10000, interfaceClass = IQuestionGroupService.class)
public class QuestionGroupServiceImpl extends ServiceImpl<QuestionGroupMapper, QuestionGroup> implements IQuestionGroupService {
    @Autowired
    QuestionGroupMapper questionGroupMapper;


    @Override
    public ServerResponse<List<QuestionGroup>> getQuestionGroupPage() {
        return ServerResponse.createBySuccess(questionGroupMapper.selectList(null));
    }

    @Override
    public ServerResponse<List<QuestionGroup>> getQuestionGroupById(Integer groupId) {
        if (Objects.isNull(groupId)) {
            return this.getQuestionGroupPage();
        }
        QuestionGroup questionGroup = new QuestionGroup();
        questionGroup.setTypeModelId(groupId);
        EntityWrapper<QuestionGroup> entityWrapper = new EntityWrapper(questionGroup);
        return ServerResponse.createBySuccess(questionGroupMapper.selectList(entityWrapper));

    }

    @Override
    public ServerResponse<List<QuestionGroup>> getByName(String name) {
        if (Strings.isBlank(name)) {
           return this.getQuestionGroupPage();
        }
        EntityWrapper<QuestionGroup> entityWrapper = new EntityWrapper<>();
        entityWrapper.like("group_title",name);
        return ServerResponse.createBySuccess(questionGroupMapper.selectList(entityWrapper));
    }

    public ServerResponse<Boolean> verificationParam(QuestionGroup questionGroup) {
        if (Objects.isNull(questionGroup.getQuestionIds())) {
            return ServerResponse.createByErrorMesage("QuestionIds is null");
        }
        if (Objects.isNull(questionGroup.getGroupDescribe())) {
            return ServerResponse.createByErrorMesage("GroupDescribe is null");
        }
        if (Objects.isNull(questionGroup.getGroupTitle())) {
            return ServerResponse.createByErrorMesage("GroupTitle is null");
        }
        if (Objects.isNull(questionGroup.getTypeModelId())) {
            return ServerResponse.createByErrorMesage("TypeModelId is null");
        }
        return ServerResponse.createBySuccessMessage("success");
    }
}
