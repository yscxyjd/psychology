package net.yscxy.service.serviceImpl;

import net.yscxy.service.config.ServerResponse;
import net.yscxy.service.entity.AnswerResult;
import net.yscxy.service.mapper.AnswerResultMapper;
import net.yscxy.service.service.IAnswerResultService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ShaoXuan
 * @since 2022-04-05
 */
@Service
@com.alibaba.dubbo.config.annotation.Service(version = "1.0.0", timeout = 10000, interfaceClass = IAnswerResultService.class)
public class AnswerResultServiceImpl extends ServiceImpl<AnswerResultMapper, AnswerResult> implements IAnswerResultService {
    @Autowired
    AnswerResultMapper answerResultMapper;

    @Override
    public ServerResponse<Boolean> addAnswerResult(AnswerResult answerResult) {
        ServerResponse<Boolean> verificationResponse = verificationParam(answerResult);
        if (!verificationResponse.isSuccess()) {
            return verificationResponse;
        }
        try {
            if (answerResultMapper.insert(answerResult) == 1) {
                return ServerResponse.createByErrorMesage("add success!");
            } else {
                return ServerResponse.createByErrorMesage("add error!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ServerResponse.createByErrorMesage("add exception!");
        }
    }

    @Override
    public ServerResponse<AnswerResult> getAnsterResult(Long userId) {
        return ServerResponse.createBySuccess(answerResultMapper.selectById(userId));
    }


    public ServerResponse<Boolean> verificationParam(AnswerResult answerResult) {
        if (Objects.isNull(answerResult.getAnswerResult())) {
            return ServerResponse.createByErrorMesage("AnswerResult is null");
        }
        if (Objects.isNull(answerResult.getAnswerTab())) {
            return ServerResponse.createByErrorMesage("AnswerTab is null");
        }
        if (Objects.isNull(answerResult.getUserId())) {
            return ServerResponse.createByErrorMesage("UserId is null");
        }
        if (Objects.isNull(answerResult.getQuestionGroupId())) {
            return ServerResponse.createByErrorMesage("QuestionGroupId is null");
        }
        return ServerResponse.createBySuccessMessage("success");
    }

}
