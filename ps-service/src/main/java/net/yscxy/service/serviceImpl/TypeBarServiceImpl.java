package net.yscxy.service.serviceImpl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import net.yscxy.service.config.ServerResponse;
import net.yscxy.service.entity.TypeBar;
import net.yscxy.service.mapper.TypeBarMapper;
import net.yscxy.service.service.IQuestionService;
import net.yscxy.service.service.ITypeBarService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ShaoXuan
 * @since 2022-04-05
 */
@Service
@com.alibaba.dubbo.config.annotation.Service(version = "1.0.0", timeout = 10000, interfaceClass = ITypeBarService.class)
public class TypeBarServiceImpl extends ServiceImpl<TypeBarMapper, TypeBar> implements ITypeBarService {
    @Autowired
    TypeBarMapper typeBarMapper;


    @Override
    public ServerResponse<List<TypeBar>> getAllTypeBar() {
        return ServerResponse.createBySuccess(typeBarMapper.selectList(new EntityWrapper<TypeBar>()));
    }

    public ServerResponse<Boolean> verificationParam(TypeBar typeBar) {
        if (Objects.isNull(typeBar.getTypeDescribe())) {
            return ServerResponse.createByErrorMesage("Describe is null");
        }
        if (Objects.isNull(typeBar.getTypeTitle())) {
            return ServerResponse.createByErrorMesage("TypeTitle is null");
        }
        if (Objects.isNull(typeBar.getTypePicture())) {
            return ServerResponse.createByErrorMesage("Picture is null");
        }
        if (Objects.isNull(typeBar.getTypeOrderNumber())) {
            return ServerResponse.createByErrorMesage("OrderNumber is null");
        }
        return ServerResponse.createBySuccessMessage("success");
    }
}
