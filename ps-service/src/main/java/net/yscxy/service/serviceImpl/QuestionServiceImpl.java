package net.yscxy.service.serviceImpl;

import net.yscxy.service.config.ServerResponse;
import net.yscxy.service.entity.Question;
import net.yscxy.service.entity.QuestionGroup;
import net.yscxy.service.entity.TypeBar;
import net.yscxy.service.mapper.QuestionMapper;
import net.yscxy.service.service.IAnswerResultService;
import net.yscxy.service.service.IQuestionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ShaoXuan
 * @since 2022-04-05
 */
@Service
@com.alibaba.dubbo.config.annotation.Service(version = "1.0.0", timeout = 10000, interfaceClass = IQuestionService.class)
public class QuestionServiceImpl extends ServiceImpl<QuestionMapper, Question> implements IQuestionService {
    @Autowired
    QuestionMapper questionMapper;

    @Autowired
    QuestionGroupServiceImpl questionGroupService;

    @Override
    public ServerResponse<List<Question>> getAllQuestions(Integer groupId) {
        QuestionGroup questionGroup = questionGroupService.selectById(groupId);
        String[] questionIds = questionGroup.getQuestionIds().split(",");
        return ServerResponse.createBySuccess(questionMapper.selectBatchIds(Arrays.asList(questionIds)));
    }


    public ServerResponse<Boolean> verificationParam(Question question) {
        if (Objects.isNull(question.getQuestionJson())) {
            return ServerResponse.createByErrorMesage("QuestionJson is null");
        }
        if (Objects.isNull(question.getQuestionType())) {
            return ServerResponse.createByErrorMesage("QuestionType is null");
        }
        if (Objects.isNull(question.getQuestionTitle())) {
            return ServerResponse.createByErrorMesage("QuestionTitle is null");
        }
        return ServerResponse.createBySuccessMessage("success");
    }


}
