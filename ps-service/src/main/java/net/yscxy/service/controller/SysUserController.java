package net.yscxy.service.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 用户信息表 前端控制器
 * </p>
 *
 * @author wangfukun
 * @since 2022-04-17
 */
@Controller
@RequestMapping("/yscxy.service/sysUser")
public class SysUserController {

}

