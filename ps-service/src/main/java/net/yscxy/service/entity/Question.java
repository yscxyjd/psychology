package net.yscxy.service.entity;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ShaoXuan
 * @since 2022-04-05
 */
public class Question extends Model<Question> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
	private Long id;
    /**
     * 问题标题
     */
	@TableField("question_title")
	private String questionTitle;
    /**
     * 具体的问题以及得分
     */
	@TableField("question_json")
	private String questionJson;
    /**
     * 问题类型标签
     */
	@TableField("question_type")
	private String questionType;
    /**
     * 创建时间
     */
	@TableField("create_time")
	private Date createTime;


	public Long getId() {
		return id;
	}

	public Question setId(Long id) {
		this.id = id;
		return this;
	}

	public String getQuestionTitle() {
		return questionTitle;
	}

	public Question setQuestionTitle(String questionTitle) {
		this.questionTitle = questionTitle;
		return this;
	}

	public String getQuestionJson() {
		return questionJson;
	}

	public Question setQuestionJson(String questionJson) {
		this.questionJson = questionJson;
		return this;
	}

	public String getQuestionType() {
		return questionType;
	}

	public Question setQuestionType(String questionType) {
		this.questionType = questionType;
		return this;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public Question setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Question{" +
			"id=" + id +
			", questionTitle=" + questionTitle +
			", questionJson=" + questionJson +
			", questionType=" + questionType +
			", createTime=" + createTime +
			"}";
	}
}
