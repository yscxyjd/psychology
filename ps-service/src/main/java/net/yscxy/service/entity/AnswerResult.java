package net.yscxy.service.entity;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ShaoXuan
 * @since 2022-04-05
 */
@TableName("answer_result")
public class AnswerResult extends Model<AnswerResult> {

    private static final long serialVersionUID = 1L;

    /**
     * 测试结果表
     */
	private Long id;
    /**
     * 测试组id
     */
	@TableField("question_group_id")
	private Long questionGroupId;
    /**
     * 用户id
     */
	@TableField("user_id")
	private Long userId;
    /**
     * 用户答案结果集
     */
	@TableField("answer_result")
	private String answerResult;
    /**
     * 用户测试类型
     */
	@TableField("answer_tab")
	private String answerTab;
    /**
     * 测试时间
     */
	@TableField("answer_time")
	private Date answerTime;


	public Long getId() {
		return id;
	}

	public AnswerResult setId(Long id) {
		this.id = id;
		return this;
	}

	public Long getQuestionGroupId() {
		return questionGroupId;
	}

	public AnswerResult setQuestionGroupId(Long questionGroupId) {
		this.questionGroupId = questionGroupId;
		return this;
	}

	public Long getUserId() {
		return userId;
	}

	public AnswerResult setUserId(Long userId) {
		this.userId = userId;
		return this;
	}

	public String getAnswerResult() {
		return answerResult;
	}

	public AnswerResult setAnswerResult(String answerResult) {
		this.answerResult = answerResult;
		return this;
	}

	public String getAnswerTab() {
		return answerTab;
	}

	public AnswerResult setAnswerTab(String answerTab) {
		this.answerTab = answerTab;
		return this;
	}

	public Date getAnswerTime() {
		return answerTime;
	}

	public AnswerResult setAnswerTime(Date answerTime) {
		this.answerTime = answerTime;
		return this;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "AnswerResult{" +
			"id=" + id +
			", questionGroupId=" + questionGroupId +
			", userId=" + userId +
			", answerResult=" + answerResult +
			", answerTab=" + answerTab +
			", answerTime=" + answerTime +
			"}";
	}
}
