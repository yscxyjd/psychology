package net.yscxy.service.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author ShaoXuan
 * @since 2022-04-05
 */
@TableName("question_group")
public class QuestionGroup extends Model<QuestionGroup> {

    private static final long serialVersionUID = 1L;

    /**
     * 问题组
     */
    private Long id;
    /**
     * 问题头图
     */
    @TableField("group_picture")
    private String groupPicture;
    /**
     * 测试标题
     */
    @TableField("group_title")
    private String groupTitle;
    /**
     * 问题描述
     */
    @TableField("group_describe")
    private String groupDescribe;
    /**
     * 具体的问题ids转化为数组
     */
    @TableField("question_ids")
    private String questionIds;
    /**
     * 测试类型
     */
    @TableField("type_model_id")
    private Integer typeModelId;


    /**
     * 测试类型
     */
    @TableField("done_number")
    private Integer doneNumber;


    public Long getId() {
        return id;
    }

    public QuestionGroup setId(Long id) {
        this.id = id;
        return this;
    }


    public Integer getDoneNumber() {
        return doneNumber;
    }

    public void setDoneNumber(Integer doneNumber) {
        this.doneNumber = doneNumber;
    }

    public String getGroupPicture() {
        return groupPicture;
    }

    public QuestionGroup setGroupPicture(String groupPicture) {
        this.groupPicture = groupPicture;
        return this;
    }

    public String getGroupTitle() {
        return groupTitle;
    }

    public QuestionGroup setGroupTitle(String groupTitle) {
        this.groupTitle = groupTitle;
        return this;
    }

    public String getGroupDescribe() {
        return groupDescribe;
    }

    public QuestionGroup setGroupDescribe(String groupDescribe) {
        this.groupDescribe = groupDescribe;
        return this;
    }

    public String getQuestionIds() {
        return questionIds;
    }

    public QuestionGroup setQuestionIds(String questionIds) {
        this.questionIds = questionIds;
        return this;
    }

    public Integer getTypeModelId() {
        return typeModelId;
    }

    public QuestionGroup setTypeModelId(Integer typeModelId) {
        this.typeModelId = typeModelId;
        return this;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "QuestionGroup{" +
                "id=" + id +
                ", groupPicture='" + groupPicture + '\'' +
                ", groupTitle='" + groupTitle + '\'' +
                ", groupDescribe='" + groupDescribe + '\'' +
                ", questionIds='" + questionIds + '\'' +
                ", typeModelId=" + typeModelId +
                ", doneNumber=" + doneNumber +
                '}';
    }
}
