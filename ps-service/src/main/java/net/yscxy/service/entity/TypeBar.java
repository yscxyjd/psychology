package net.yscxy.service.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author ShaoXuan
 * @since 2022-04-05
 */
@TableName("type_bar")
public class TypeBar extends Model<TypeBar> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id-此表用来显示tab标签
     */
	private Integer id;
    /**
     * 类型名称
     */
	@TableField("type_title")
	private String typeTitle;
    /**
     * 类型图标
     */
	@TableField("type_picture")
	private String typePicture;
    /**
     * 类型描述
     */
	@TableField("type_describe")
	private String typeDescribe;
    /**
     * 排序权重越小越靠前
     */
	@TableField("type_order_number")
	private Integer typeOrderNumber;


	public Integer getId() {
		return id;
	}

	public TypeBar setId(Integer id) {
		this.id = id;
		return this;
	}

	public String getTypeTitle() {
		return typeTitle;
	}

	public TypeBar setTypeTitle(String typeTitle) {
		this.typeTitle = typeTitle;
		return this;
	}

	public String getTypePicture() {
		return typePicture;
	}

	public TypeBar setTypePicture(String typePicture) {
		this.typePicture = typePicture;
		return this;
	}

	public String getTypeDescribe() {
		return typeDescribe;
	}

	public TypeBar setTypeDescribe(String typeDescribe) {
		this.typeDescribe = typeDescribe;
		return this;
	}

	public Integer getTypeOrderNumber() {
		return typeOrderNumber;
	}

	public TypeBar setTypeOrderNumber(Integer typeOrderNumber) {
		this.typeOrderNumber = typeOrderNumber;
		return this;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "TypeBar{" +
			"id=" + id +
			", typeTitle=" + typeTitle +
			", typePicture=" + typePicture +
			", typeDescribe=" + typeDescribe +
			", typeOrderNumber=" + typeOrderNumber +
			"}";
	}
}
