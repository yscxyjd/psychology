package net.yscxy.service.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author WangFuKun
 * @since 2021-03-08
 */
@TableName("m_key")
public class MKey extends Model<MKey> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 卡密
     */
	@TableField("k_message")
	private String kMessage;
    /**
     * 创建时间
     */
	@TableField("create_time")
	private Date createTime;
    /**
     * 是否已经使用
     */
	@TableField("is_used")
	private Integer isUsed;
    /**
     * 过期时间
     */
	@TableField("overdue_time")
	private Date overdueTime;
    /**
     * 谁创建的
     */
	@TableField("create_user_id")
	private Integer createUserId;
    /**
     * 激活者的机器码
     */
	@TableField("m_id")
	private String mId;
    /**
     * 使用时长,单位是天
     */
	@TableField("time_long")
	private Integer timeLong;
    /**
     * 激活码的类型，0不能群发，1能群发
     */
	@TableField("k_type")
	private String kType;


	public Integer getId() {
		return id;
	}

	public MKey setId(Integer id) {
		this.id = id;
		return this;
	}

	public String getkMessage() {
		return kMessage;
	}

	public MKey setkMessage(String kMessage) {
		this.kMessage = kMessage;
		return this;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public MKey setCreateTime(Date createTime) {
		this.createTime = createTime;
		return this;
	}

	public Integer getIsUsed() {
		return isUsed;
	}

	public MKey setIsUsed(Integer isUsed) {
		this.isUsed = isUsed;
		return this;
	}

	public Date getOverdueTime() {
		return overdueTime;
	}

	public MKey setOverdueTime(Date overdueTime) {
		this.overdueTime = overdueTime;
		return this;
	}

	public Integer getCreateUserId() {
		return createUserId;
	}

	public MKey setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
		return this;
	}

	public String getmId() {
		return mId;
	}

	public MKey setmId(String mId) {
		this.mId = mId;
		return this;
	}

	public Integer getTimeLong() {
		return timeLong;
	}

	public MKey setTimeLong(Integer timeLong) {
		this.timeLong = timeLong;
		return this;
	}

	public String getkType() {
		return kType;
	}

	public MKey setkType(String kType) {
		this.kType = kType;
		return this;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "MKey{" +
			"id=" + id +
			", kMessage=" + kMessage +
			", createTime=" + createTime +
			", isUsed=" + isUsed +
			", overdueTime=" + overdueTime +
			", createUserId=" + createUserId +
			", mId=" + mId +
			", timeLong=" + timeLong +
			", kType=" + kType +
			"}";
	}
}
