package net.yscxy.service.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import net.yscxy.service.entity.MKey;

import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author WangFuKun
 * @since 2021-03-08
 */
@Mapper
public interface MKeyMapper extends BaseMapper<MKey> {

}
