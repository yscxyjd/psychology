package net.yscxy.service.mapper;

import net.yscxy.service.entity.QuestionGroup;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ShaoXuan
 * @since 2022-04-05
 */
@Mapper
public interface QuestionGroupMapper extends BaseMapper<QuestionGroup> {

}
